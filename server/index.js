'use strict';

const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const mongodb = require('mongodb');
const fs = require('fs');

const port = process.env.PORT || 3000;

const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server);

const MongoClient = mongodb.MongoClient;
const url = 'mongodb://localhost:27017/drone';
let collection, db;

app.use(express.static(__dirname + '/../app')); // __dirname - абсолют.путь к этому файлу

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(function (req, res, next) {
  console.log(123);
  MongoClient.connect(url, function (err, dBase) {
    if (err) {
      console.log('Невозможно подключиться к базе данных MongoDB. Ошибка ', err)
    } else {
      console.log('Соединение установлено для ', url);
      db = dBase; // 1 соединение на сессию, но разные на другие подключения
    }

    next();
  });
});


app.post('/', function (req, res) {

  let currentUser = req.body;
  console.log(currentUser);
  const collection = db.collection('users');

  //const collection = dBase.collection('users');

  collection.find().toArray((err, result) => {
    if(err) {
      console.log(err);
    } else if (!checkUser(result, currentUser.email)) {
      currentUser.credits = 100;
      collection.insert(currentUser);
    } else if (currentUser.name) {  // for case when name is absent into previous state
      collection.update({email: currentUser.email}, {$set: {name: currentUser.name}});
    }
    //console.log(checkUser(result));
    collection.find({email: currentUser.email}).toArray((err, result) => {
      if (err) {
        console.log(err);
      } else {
        currentUser = result;
        //console.log(currentUser);
        res.send(currentUser[0]);

        // db.close(); не работает
      }
    });

  });
  showUsers(collection);

  console.log('post / 66');
});

app.get('/menu', function (req, res) {

  fs.readFile('menu.json', 'utf8', (err, data) => {
    res.send(data);
  });

  console.log('get menu 79');
});

app.put('/user', function (req, res) {
  const collection = db.collection('users');
  console.log(req.body);
  collection.update({email: req.body.email}, {$set: {credits: req.body.credits}});

  console.log('put user 87');
  res.send(req.body);
});

app.get('/dishes', function (req, res) {

  const collection = db.collection('dishes');

  collection.find().toArray((err, result) => {
    if (err) {
      console.log(err);
    } else {
      //let dishes = formatDishes(result[0]);
      res.send(result);
    }
  });

  console.log('get dishes 104');
});

app.post('/dishes', function (req, res) {

  let _id = req.body._id;
  console.log('103', req.body);

  const collection = db.collection('dishes');
  collection.find({_id}).toArray((err, result) => {

    if (result.length) {
      collection.update({_id}, {$push: {dishes: req.body.dish}});

    } else {
      const dishes = [];
      dishes.push(req.body.dish); //console.log('107', dishes);
      collection.insert({_id, dishes});
    }

    collection.find({_id}).toArray((err, result1) => {
      console.log('119', result1);
      res.send(result1[0]);
    });
  });

  console.log('post dishes 130');
});

app.put('/dishes', function (req, res) {
  const collection = db.collection('dishes');

  /*collection.find({_id: req.body._id}).toArray((err, result) => {
    const targetIndex = result[0].dishes.findIndex((dish) => {
      return dish.id === req.body.dishID;
    });*/

  console.log('132', req.body);
  //collection.update({_id: req.body._id}, {$set: {`dishes.${targetIndex}.status`: req.body.status}});
  collection.update({$and: [{_id: req.body._id},{"dishes.id": req.body.dishID}]},
    {$set: {"dishes.$.status": req.body.status}});

  collection.find({_id: req.body._id}).toArray((err, result) => {
    console.log('138', result[0]);
    res.send(result[0].dishes);
  });

  console.log('put dishes 151');
  });


//app.listen(port, function () {  // http://stackoverflow.com/questions/10191048/socket-io-js-not-found/10192084#10192084
server.listen(port, function () {
  console.log('HTTP server started on %d port', port);
});

function formatDishes(result) {
  //const dishes = {};

  let dishes = result.map((item) => {
    return dishes[item.email] = item.dishes;
  });
  return dishes;
}

function checkUser(users, userEmail) {
  return users.find((item) => {
    return item.email === userEmail
  });
}

function showUsers(collection) {
  collection.find().toArray(function (err, result) {
    if(err) {
      console.log(err);
    } else if(result.length) {
      console.log('DB contains : ', result);
    } else {
      console.log('Нет документов с данным условием поиска.');
    }
  });
}

io.on('connection', (socket) => {
  console.log('----------',socket.id);
  console.log('----------',Object.keys(io.sockets.connected).length); // кол-во подключений

  socket.on('fromKitchen', function () {
    socket.broadcast.emit('fromKitchen1');
    console.log('socket.io fromKitchen');
  });
  socket.on('fromUser', function () {
    socket.broadcast.emit('fromUser1');
    console.log('socket.io fromUser');
  });

  socket.on('disconnect', function() {
    console.log('[Socket.IO] User disconnected! Socket ID: ' + socket.id);
  });
});