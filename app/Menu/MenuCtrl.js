'use strict';
angular
  .module('myApp')
  .controller('MenuCtrl', function (ConnectionService, $scope, $rootScope, OrdersStore, UserStore, socket) {

    ConnectionService.getMenu().then((menu) => {
      this.menu = menu.data;
    });

    //this.currentUser = $rootScope.currentUser;
    //this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    this.currentUser = UserStore.getUser();

    this.addOrderedDish = function (dish) {
      //$rootScope.$broadcast('add dash', dash);
      //$rootScope.currentUser.credits -= dash.price;
      //OrdersStore.addOrderedDish(dish);
      OrdersStore.setCurrentUserDish(dish).then(() => {
        $scope.$state.go('user');
        console.log(dish);
      });


      this.currentUser.credits -= dish.price;
      UserStore.changeUserCredits(this.currentUser);
    }
  });
