angular
  .module('myApp')
  .component('authComponent', {
    templateUrl: 'Auth/AuthComponent.html',
    controller: function (ConnectionService, $location, $rootScope, UserStore, $sessionStorage, $state) {
      //this.currentUser = {};

      this.authorization = function (user) {

        ConnectionService.authUser(user).then((usersData) => {

          this.currentUser = usersData.data;
          //sessionStorage.setItem('currentUserID', usersData.data._id);
          $sessionStorage.currentUser = usersData.data;
          UserStore.setUser(usersData.data);
          //this.currentUser = UserStore.getUser();
          //console.log(sessionStorage.getItem('currentUserID'));

          //$location.path('/account');

          $rootScope.$broadcast('auth', this.currentUser);
          //$rootScope.currentUser = this.currentUser;
          $state.go('user');
        });
      }
    }
  });
