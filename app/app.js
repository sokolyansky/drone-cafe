'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ui.router',
  'myApp.auth',
  'ngStorage',
  'btford.socket-io'  //https://www.npmjs.com/package/angular-socket-io
])
  .run(function ($rootScope, $state, $stateParams, SessionService, OrdersStore, socket) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    $rootScope.currentUser = null;

    OrdersStore.getDishes();
    console.log('---  RUN');
    socket.on('connect', function() {
      console.log('Connected to server.');
    });
    // Здесь мы будем проверять авторизацию
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      SessionService.checkAccess(event, toState, toParams, fromState, fromParams);
    });
  })
  .config(['$locationProvider', '$stateProvider', '$urlRouterProvider',
    function($locationProvider, $stateProvider, $urlRouterProvider) {
      $locationProvider.hashPrefix('!');

      $stateProvider
        .state({
          name: 'home',
          url: '/',  // корневой state должен быть описан
          templateUrl: 'user/userDashboard.html'
        })
        .state({
          name: 'user',
          url: '/user',
          templateUrl: 'user/userDashboard.html'
        })
        .state({
          name: 'account',
          /*url: '/account',*/
          templateUrl: 'Account/Account.html',
          controller: 'AccountCtrl as vm'
        })
        .state({
          name: 'home.menu',
          url: 'menu',
          templateUrl: 'Menu/Menu.html',
          controller: 'MenuCtrl as menu'
        })
        .state({
          name: 'kitchen',
          url: '/kitchen',
          templateUrl: 'kitchen/kitchen.html',
          controller: 'KitchenCtrl as vm',
          data: {
            noLogin: true
          }
        });

    }])

  .controller('MainCtrl', function ($scope, $window, UserStore, $sessionStorage) {

    //this.authorized = $window.sessionStorage.getItem('authorized');
    this.authorized = $sessionStorage.authorized;

    $scope.$on('auth', (event, data) => {
      this.authorized = true;
      //$window.sessionStorage.setItem('authorized', true);
      $sessionStorage.authorized = true;
    });

    window.onbeforeunload = function (event) {
      //console.log(UserStore.getUser().credits);
      const credits = UserStore.getUser().credits;
      UserStore.saveUserCredits(credits); // функция не срабатывает при onbeforeunload и onunload
    }
  });
