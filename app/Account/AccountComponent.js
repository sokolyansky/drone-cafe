angular
  .module('myApp')
  .component('accountComponent', {
    templateUrl: 'Account/AccountComponent.html',
    controller: function ($scope, $window, UserStore) {
      //this.currentUser = $scope.$parent.main.currentUser;
      //console.log($scope.$parent.main.currentUser);
      //this.currentUser = JSON.parse($window.sessionStorage.getItem('currentUser'));
      this.currentUser = UserStore.getUser();
      //console.log(UserStore.getUser());

      this.chargeAccount = function (amount) {
        this.currentUser.credits += amount;
        UserStore.changeUserCredits(this.currentUser);
      };

      $scope.$on('auth', (event, data) => {
        this.currentUser = data;
      });
    },
    controllerAs: 'account'
  });
