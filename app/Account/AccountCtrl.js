'use strict';

angular
  .module('myApp')
  .controller('AccountCtrl', function ($scope, $rootScope, ConnectionService) {
    this.currentUser = $rootScope.currentUser;
    console.log(this.currentUser);

    $scope.$on('auth', (event, data) => {
      console.log(33);
    });

    this.getMenu = function () {
      ConnectionService.getMenu().then((menu) => {
        this.menu = menu;
        console.log(this.menu);
      })
    }
  });
