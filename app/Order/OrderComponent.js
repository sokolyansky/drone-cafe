angular
  .module('myApp')
  .component('orderComponent', {
    templateUrl: 'Order/Order.html',
    controller: function ($scope, $rootScope, OrdersStore, socket) {
      this.dishes = OrdersStore.getCurrentUserDishes();

      $scope.$on('reload', () => {  // для мгновенного обновления не подходит, т.к. $rootScope для разных экземпляров приложения - разные.
        this.dishes = OrdersStore.getCurrentUserDishes(); // в 2-х разных вкладках браузера будут разные экземпляры приложения (и разные $rootScope)
        // console.log('$rootScope')
      });

      socket.on('fromKitchen1', () => { // для мгновенного обновления нужно использовать фреймверк для взаимодействия в реальном времени like Socket.io
        console.log('ssssocket');  // для связи с серверным socket.io(обычным) нужен angular-socket-io
        OrdersStore.getDishes().then(() => {
          this.dishes = OrdersStore.getCurrentUserDishes();
        });
      });

      /*$scope.$on('add dash', (event, dash) => {
        dash.status = 'Заказано';
        $rootScope.currentUser.credits -= dash.price;

        this.dashes.push(dash);
        console.log(dash);
      });*/

    },
    controllerAs: 'order'
  });
