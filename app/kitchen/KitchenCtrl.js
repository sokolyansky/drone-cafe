'use strict';

angular
  .module('myApp')
  .controller('KitchenCtrl', function ($localStorage, OrdersStore, $scope, socket, $timeout) {
    /*this.orderedDishes = $localStorage.dishes.filter((dish) => {
      return dish.status === 'Заказано';
    });*/
    /*this.preparingDishes = $localStorage.dishes.filter((dish) => {
      return dish.status === 'Готовится';
    });*/

    this.orderedDishes = OrdersStore.getOrderedDish();
    console.log(this.orderedDishes);
    
    this.startToPrepare = function (id, dishID) {
      OrdersStore.transferToPreparing(id, dishID);

    };
    this.done = function (id, dishID) {
      OrdersStore.done(id, dishID);

    };

    socket.on('fromUser1', () => {
      OrdersStore.getDishes().then(() => {
        this.orderedDishes = OrdersStore.getOrderedDish();
        console.log('fromUser');
        console.log(this.orderedDishes);
      });

      /*$timeout(() => {
        this.orderedDishes = OrdersStore.getOrderedDish();
        console.log('fromUser');
        console.log(this.orderedDishes);
      },1000);*/

    });

    $scope.$watch('$localStorage.dishes.status', function (newValue, oldValue, scope) {
      /*this.orderedDishes = $localStorage.dishes.filter((dish) => {
        return dish.status === 'Заказано';
      });
      this.preparingDishes = $localStorage.dishes.filter((dish) => {
        return dish.status === 'Готовится';
      });*/

    });
  });
