'use strict';

angular
  .module('myApp.auth')
  .factory('SessionService', function ($injector, OrdersStore) {
    
    return {
      checkAccess: function (event, toState, toParams, fromState, fromParams) {

        let $scope = $injector.get('$rootScope'),
          $sessionStorage = $injector.get('$sessionStorage');

        //console.log($sessionStorage);
        if (toState.data !== undefined) {
          if (toState.data.noLogin !== undefined && toState.data.noLogin) {
            // если нужно, выполняйте здесь какие-то действия
            // перед входом без авторизации
          }
        } else {
          if ($sessionStorage.currentUser) {
            $scope.$root.currentUser = $sessionStorage.currentUser;
            //OrdersStore.getDishes(); console.log(111);
          } else {
            event.preventDefault();
            $scope.$state.go('auth.login');
          }
        }
      }
    }
  });
