'use strict';

angular
  .module('myApp')
  .factory('UserStore', function ($http, $sessionStorage) {
    //let currentUser;
    $sessionStorage.currentUser;

    return {
      getUser: function () {
        return $sessionStorage.currentUser;
      },
      setUser: function (user) {
        $sessionStorage.currentUser = user;
      },
      changeUserCredits: function (user) {
        console.log(user);
        return $http.put('/user', user)
          .then(function success(result) {
            $sessionStorage.currentUser.credits = result.data.credits;
        })
      },
      getUserCredits: function (user) {
        return $http.get('/user' + user._id);
      }
    }
  });
