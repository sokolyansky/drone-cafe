'use strict';
angular
  .module('myApp')
  .factory('OrdersStore', function ($localStorage, $sessionStorage, $http, $rootScope, socket) {
    let dishes;  // должен быть 1 массив, чтобы изменения видели пользователи и кухня
    //let allDishes = [];

    //const dishes = [];  // приватная переменная (в методе по замыканию)
    // $localStorage.dishes - при выходе инфо о заказанный блюдах сохраняется
    // используется для хранения сервис, т.к. он - синглтон(где бы он не вызывался, возвращается один и тот же экземпляр, который имеет одну и ту же приватную переменную state)
    // вместо сервиса для хранения можно было использовать $rootScope или отправлять в нужный scope по событию.
    return {

      addOrderedDish(dish) {
        dish.status = 'Заказано';
        $localStorage.dishes.push(dish);
      },


      getDishes() { // it returns Promise

        return $http.get('/dishes')
          .then(function success(data) {
            if (!data.data.length) {
              dishes = [];
              return;
            }

            dishes = data.data;
            console.log(dishes);
        })
      },
      getCurrentUserDishes() {
        console.log(dishes);

        const currentUserDishes = dishes.find((item) => {
          return item._id == $sessionStorage.currentUser.email;
        });

        if (currentUserDishes) {
          return currentUserDishes.dishes;
        }
      },
      setCurrentUserDish(dish) {
        dish.status = 'Заказано';
        const obj = {
          _id: $sessionStorage.currentUser.email,
          dish
        };

        return $http.post('/dishes', obj)
          .then((data) => {

            const currentUserDishes = dishes.find((item) => {
              return item._id === data.data._id;
            });
            if (currentUserDishes) {
              currentUserDishes.dishes = data.data.dishes;
            } else {
              dishes.push(data.data);
            }
            $rootScope.$broadcast('reload');
            socket.emit('fromUser');
            console.log(data.data);
          })
      },

      /*getAllDishes() {

        return $http.get('/dishes')
          .then(function success(data) {
            //allDishes = data.data;
            data.data.forEach((item) => {
              allDishes = allDishes.concat(item.dishes);
            });
          })
      },*/
      getOrderedDish() {
        /*let arr = [];
        dishes.forEach((item) => {
          arr = arr.concat(item.dishes);
        });*/

        return dishes;
      },
      transferToPreparing(_id, dishID) {
        /*const targetUser = dishes.find((item) => {
          return item.id == id;
        });
        const targetDish = targetUser.dishes.find((item) => {
          return item == dish;
        });
        targetDish.status = 'Готовится';
        console.log(targetDish);*/
        const status = 'Готовится';
        return $http.put('/dishes', {_id, dishID, status})
          .then(function success(data) {
            const target = dishes.find((item) => {
              return item._id === _id;
            });
            target.dishes = data.data;
            console.log(target);
            socket.emit('fromKitchen');
          })
      },
      done(_id, dishID) {
        const status = 'Доставляется';

        return $http.put('/dishes', {_id, dishID, status})
          .then(function success(data) {
            const target = dishes.find((item) => {
              return item._id === _id;
            });
            target.dishes = data.data;
            console.log(target);
            socket.emit('fromKitchen');
          })
      }
    }
  });
