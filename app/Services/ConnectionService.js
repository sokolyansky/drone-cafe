angular
  .module('myApp')
  .factory('ConnectionService', function ($http) {

    return {
      getUsers: function () {
        return $http.get('db.json');
      },
      setUser: function (user) {
        return $http.post('db.json', user);
      },

      authUser: function (user) {
        return $http.post('/', user);
      },
      getMenu: function () {
        return $http.get('/menu');
      },
      getDishes: function () {
        return $http.get('/dishes');
      }
    }
  });
