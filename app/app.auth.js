'use strict';
angular
  .module('myApp.auth', ['ui.router'])
  .config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

      $stateProvider
        .state({
          name: 'auth',
          url: '/auth',
          abstract: true,
          template: '<ui-view></ui-view>'
        })
        .state({
          name: 'auth.login',
          url: '/login',
          data: {
            noLogin: true
          },
          template: '<auth-component></auth-component>'
        });

      $urlRouterProvider.otherwise('/');
    }]);